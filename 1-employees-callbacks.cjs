const fs = require('fs');
const { default: _ } = require('underscore');

let data;
fs.readFile('./data.json', 'utf-8', (err, dataJSON) => {
    if (err)
        console.error(err);
    else {
        console.log('data read done');
        data = JSON.parse(dataJSON);
        let res = data.employees.filter((employee) => {
            if (employee.id == 2 || employee.id == 13 || employee.id == 23)
                return true;
            else
                return false;
        });
        fs.writeFile('./output/1-Retrieve-data-for-ids.json', JSON.stringify(res), (err) => {
            if (err)
                console.error(err);
            else {
                console.log("1. Retrieve-data-for-ids 2,12 23 done");
                let res = data.employees.reduce((eachCompany, employee) => {
                    if (eachCompany.hasOwnProperty(employee.company))
                        eachCompany[employee.company].push(employee);
                    else {
                        eachCompany[employee.company] = [];
                        eachCompany[employee.company].push(employee);
                    }
                    return eachCompany;
                }, {});
                fs.writeFile('./output/2-Group-data-based-on-companies.json', JSON.stringify(res), (err) => {
                    if (err)
                        console.error(err);
                    else {
                        console.log("2-Group-data-based-on-companies done");
                        let res = data.employees.filter((employee) => {
                            if (employee.company == 'Powerpuff Brigade')
                                return true;
                            else
                                return false;
                        });
                        fs.writeFile('./output/3-data-Powerpuff-Brigade.json', JSON.stringify(res), (err) => {
                            if (err)
                                console.error(err);
                            else {
                                console.log("3. Get all data for company Powerpuff Brigade done");
                                let res = data.employees.filter((employee) => {
                                    if (employee.id != 2)
                                        return true;
                                    else
                                        return false;
                                });
                                fs.writeFile('./output/4-Remove-id-2.json', JSON.stringify(res), (err) => {
                                    if (err)
                                        console.error(err);
                                    else {
                                        console.log("4. Remove entry with id 2 done");
                                        let res = data.employees.sort((employee1, employee2) => {
                                            if (employee1.company > employee2.company)
                                                return 1;
                                            else if (employee1.company < employee2.company)
                                                return -1;
                                            else {
                                                if (employee1.id > employee2.id)
                                                    return 1;
                                                else if (employee1.id < employee2.id)
                                                    return -1;
                                                else
                                                    return 0;
                                            }
                                        })
                                        fs.writeFile('./output/5-sort-data.json', JSON.stringify(res), (err) => {
                                            if (err)
                                                console.error(err);
                                            else {
                                                console.log("5.Sort data based on company name and id done.");
                                                let id93Index = data.employees.findIndex(employee => employee.id == 93);
                                                let id92Index = data.employees.findIndex(employee => employee.id == 92);
                                                let temp = data.employees[id92Index];
                                                data.employees[id92Index] = data.employees[id93Index];
                                                data.employees[id93Index] = temp;
                                                fs.writeFile('./output/6-swap-93-and-92.json', JSON.stringify(data.employees), (err) => {
                                                    if (err)
                                                        console.error(err);
                                                    else {
                                                        console.log("6. Swap position of companies with id 93 and id 92 done");
                                                        let res = data.employees.map((employee) => {
                                                            if (employee.id % 2 == 0) {
                                                                employee.birthday = new Date().toDateString();
                                                            }
                                                            return employee;
                                                        });
                                                        fs.writeFile('./output/7-add-birthday-to-even-id.json', JSON.stringify(res), (err) => {
                                                            if (err)
                                                                console.error(err);
                                                            else {
                                                                console.log("7. Adding birthday to even id employees done");
                                                            }
                                                        })
                                                    }
                                                });
                                            }
                                        })
                                    }
                                })
                            }
                        });
                    }
                })
            }
        })
    }
})